from threading import Thread
from queue import Queue, Empty


def worker(q):
    while True:

        try:
            item = q.get(block=True, timeout=1)

            q.task_done()
            if item == "quit":
                print("got quit msg in thread")
                break

        except Empty:
            print("empty, do some arduino stuff")


def input_process(q):
    while True:
        x = input("")

        if x == 'q':
            print("will quit")
            q.put("quit")
            break

q = Queue()
t = Thread(target=worker, args=(q,))
t.start()

t2 = Thread(target=input_process, args=(q,))
t2.start()

# waits for the `task_done` function to be called
q.join()

t2.join()
t.join()